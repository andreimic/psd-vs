package uk.ac.glasgow.demosocs;

import java.util.List;

public interface ElectionResult {
	/**
	 * @return a list of candidates who were successful in the election, in the
	 *         order in which they were elected.
	 */
	public List<Option> getWinningOptions();
}
