package uk.ac.glasgow.demosocs;

/**
 * Provides information about a generic option in an election.
 * @author tws
 *
 */
public interface Option {
	public String getDisplayableDescription ();
}
