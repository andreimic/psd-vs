package uk.ac.glasgow.demosocs;

/**
 * An abstract vote, representing an instance that may be cast in an election.
 * @author tws
 *
 */
public interface Vote {

}
