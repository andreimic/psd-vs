package uk.ac.glasgow.demosocs.impl.ui;

import java.io.InputStream;
import java.io.OutputStream;

import uk.ac.glasgow.clui.CommandLineUI;
import uk.ac.glasgow.demosocs.VotingSystem;
import uk.ac.glasgow.demosocs.impl.VotingSystemStub;

/**
 * Extends the Command Line User Interface framework for the voting system.
 * @author tws
 *
 */
public class VotingSystemCommandLineUI extends CommandLineUI<VotingSystem>{

	public VotingSystemCommandLineUI(
			VotingSystem facade,
			InputStream in,
			OutputStream out,
			OutputStream err) {
		
		super(facade, in, out,err);
	}
	
	/**
	 * Constructs a new command line interface tailored for commands for the Voting System facade.
	 * @param args
	 */
	public static void main(String[] args){
		
		VotingSystem facade = new VotingSystemStub();
		
		CommandLineUI<VotingSystem> vsclui = new VotingSystemCommandLineUI (
				facade,
				System.in,
				System.out,
				System.err);
			
			vsclui.addSystemCommand("user", new SetCurrentUserCommand(facade));
			vsclui.addSystemCommand("election", new SetCurrentElectionCommand(facade));
			
			vsclui.addSystemCommand("create", new CreateElectionCommand(facade));
			vsclui.addSystemCommand("edit", new EditElectionCommand(facade));
			vsclui.addSystemCommand("elections", new GetElectionsCommand(facade));

			vsclui.addSystemCommand("nominate", new NominateCandidateCommand(facade));
			vsclui.addSystemCommand("accept", new AcceptNominationCommand(facade));
			vsclui.addSystemCommand("nominations", new GetNominationCommand(facade));

			vsclui.addSystemCommand("cast", new CastVoteCommand(facade));
			
			vsclui.addSystemCommand("publish", new PublishResultsCommand(facade));

			new Thread(vsclui).start();
	}

}
