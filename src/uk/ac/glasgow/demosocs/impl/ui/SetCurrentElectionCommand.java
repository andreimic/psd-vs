package uk.ac.glasgow.demosocs.impl.ui;

import uk.ac.glasgow.clui.ArgumentsDescription;
import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(1)
@CommandDescription("Sets the currently selected election.")
@ArgumentsDescription("<election>")
public class SetCurrentElectionCommand extends SystemCommand<VotingSystem> {

	public SetCurrentElectionCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		
		String electionID = arguments[0];
		facade.setCurrentElection(electionID);
				
		Election election = facade.getCurrentElection();
				
		
		if (election == null || !election.getEID().equals(electionID)){
			throw new SystemCommandException(
					"The current election could not be set to ID ["+
					electionID+"] - is the ID valid?");
		}else{
			return "The current election was set to ID ["+electionID+"]";
		}
			
	}

}
