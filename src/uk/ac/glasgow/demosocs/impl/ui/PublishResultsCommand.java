package uk.ac.glasgow.demosocs.impl.ui;

import java.util.List;

import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.ElectionResult;
import uk.ac.glasgow.demosocs.Option;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(0)
@CommandDescription("Publish the results of the currently selected election.")
public class PublishResultsCommand extends SystemCommand<VotingSystem> {

	public PublishResultsCommand(VotingSystem facade) {
		super(facade);
	}
	
	@Override
	public String processCommand(String[] arguments)
		throws SystemCommandException {
		
		Election election = facade.getCurrentElection();
		
		if (election == null)
			throw new SystemCommandException("No current election set.");
		
		facade.publishResults();
		
		ElectionResult result = facade.getCurrentElection().getElectionResult();
		
		if (result != null){
			List<Option> options = result.getWinningOptions();
			return "Candidate ["+options.get(0).getDisplayableDescription()+"] won election ["+election.getEID()+"].";
		} else {
			return "The election results for ["+election.getEID()+"] could not be obtained.";
		}
	}

}
