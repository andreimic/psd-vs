package uk.ac.glasgow.demosocs.impl.ui;

import java.util.Set;

import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(0)
@CommandDescription("List all nominations for the currently selected user.")
public class GetNominationCommand extends SystemCommand<VotingSystem> {

	public GetNominationCommand(VotingSystem facade) {
		super(facade);
	}
	
	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		
		String guid = facade.getCurrentUserGUID();
		if (guid == null)
			throw new SystemCommandException("No current user set.");
		
		String result = "EID\n";
		
		Set<Election> nominations = facade.getNominations();
		
		for (Election nomination: nominations)
			result+=
				nomination.getEID()+"\n";
		
		return result;	
	}

}
