package uk.ac.glasgow.demosocs.impl.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

import uk.ac.glasgow.clui.SystemCommandException;

public class DateUtil {
	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	// TODO: may need to change the format to include time of day.
	
	public static Date parseArgumentDate(String day, String time) throws SystemCommandException {
		
		System.out.println(df.format(new Date()));
		
		String dateString = day+" "+time;
		try {
			return df.parse(dateString);
		} catch (ParseException e) {
			throw new SystemCommandException("Couldn't parse date string: ["+dateString+"]");
		}
	}
}
