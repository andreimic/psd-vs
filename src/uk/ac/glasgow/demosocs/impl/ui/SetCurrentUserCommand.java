package uk.ac.glasgow.demosocs.impl.ui;

import uk.ac.glasgow.clui.ArgumentsDescription;
import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(2)
@CommandDescription("Sets the currently selected user.")
@ArgumentsDescription("<guid> <password>")
public class SetCurrentUserCommand extends SystemCommand<VotingSystem> {

	public SetCurrentUserCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		
		String userName = arguments[0];
		String password = arguments[1];
		
		boolean result = facade.setCurrentUser(userName,password);
		
		if (result) return "User ["+userName+"] now the current user.";
		else return "Invalid username or password";
	}
}
