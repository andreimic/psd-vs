package uk.ac.glasgow.demosocs.impl.ui;

import java.util.Date;

import uk.ac.glasgow.clui.ArgumentsDescription;
import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(9)
@CommandDescription("Create a new election with the specified parameters.")
@ArgumentsDescription("<name> <type> <nomination day> <nomination time> <start day> <start time> <stop day> <stop time> <electorate ID>")
public class CreateElectionCommand extends SystemCommand<VotingSystem> {

	public CreateElectionCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		String name = arguments[0];
		String type = arguments[1];
		
		facade.createElection(name, type);
				
		Date openNominations = DateUtil.parseArgumentDate(arguments[2],arguments[3]);
		Date start = DateUtil.parseArgumentDate(arguments[4],arguments[5]);
		Date stop = DateUtil.parseArgumentDate(arguments[6],arguments[7]);
		
		String electorateID = arguments[8];
		
		facade.editElection(openNominations, start, stop, electorateID);

		return "Created election with name ["+name+"], type ["+type+"] and electorate ["+electorateID+"].";
	}
}
