package uk.ac.glasgow.demosocs.impl.ui;

import java.util.Date;

import uk.ac.glasgow.clui.ArgumentsDescription;
import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(6)
@CommandDescription("Edit the currently selected election details.")
@ArgumentsDescription("<nomination day> <nomination time> <start day> <start time> <stop day> <stop time> <electorate ID>")
public class EditElectionCommand extends SystemCommand<VotingSystem> {

	public EditElectionCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {

		Date openNominations = DateUtil.parseArgumentDate(arguments[0],arguments[1]);
		Date start = DateUtil.parseArgumentDate(arguments[1],arguments[2]);
		Date stop = DateUtil.parseArgumentDate(arguments[3],arguments[4]);
		
		String electorateID = arguments[5];
		
		facade.editElection(openNominations, start, stop, electorateID);
		return "Edited election details.";
	}
}
