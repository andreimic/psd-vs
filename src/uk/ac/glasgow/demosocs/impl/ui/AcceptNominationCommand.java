package uk.ac.glasgow.demosocs.impl.ui;

import java.util.Set;

import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.Candidate;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(0)
@CommandDescription("Accept a nomination for the currently selected election.")
public class AcceptNominationCommand extends SystemCommand<VotingSystem> {

	public AcceptNominationCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		
		Election election = facade.getCurrentElection();
		
		if (election == null)
			throw new SystemCommandException("No current election set.");
		
		String guid = facade.getCurrentUserGUID();
		if (guid == null)
			throw new SystemCommandException("No current user set.");
				
		facade.acceptNomination();

		Set<Candidate> candidates = facade.getCurrentElection().getCandidates();
		for (Candidate candidate: candidates){
			if (candidate.getGUID().equals(guid))
				return "Nomination for the current election ["+
					election.getEID()+"] accepted for the current user ["+guid+"].";
		}
		return "The nomination was not made, perhaps the current user ["+guid+"]" +
				" is not eligible for election ["+election.getEID()+"]?";
	}
}
