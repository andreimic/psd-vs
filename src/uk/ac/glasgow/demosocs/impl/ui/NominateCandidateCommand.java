package uk.ac.glasgow.demosocs.impl.ui;

import uk.ac.glasgow.clui.ArgumentsDescription;
import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(1)
@CommandDescription("Nominate the specified candidate in the current election.")
@ArgumentsDescription("<candidate>")
public class NominateCandidateCommand extends SystemCommand<VotingSystem> {

	public NominateCandidateCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		Election election = facade.getCurrentElection();
		
		if (election == null)
			throw new SystemCommandException("No current election set.");
		
		String candidate = arguments[0];
		
		facade.nominateCandidate(candidate);
		
		return "A nomination of candidate ["+candidate+
		"] for election ["+election.getEID()+
		"] was submitted by ["+facade.getCurrentUserGUID()+"].";
	}

}
