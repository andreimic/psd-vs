package uk.ac.glasgow.demosocs.impl.ui;

import java.util.Set;

import uk.ac.glasgow.clui.CommandDescription;
import uk.ac.glasgow.clui.MinimumArguments;
import uk.ac.glasgow.clui.SystemCommand;
import uk.ac.glasgow.clui.SystemCommandException;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.VotingSystem;

@MinimumArguments(0)
@CommandDescription("List all elections.")
public class GetElectionsCommand extends SystemCommand<VotingSystem> {

	public GetElectionsCommand(VotingSystem facade) {
		super(facade);
	}

	@Override
	public String processCommand(String[] arguments)
			throws SystemCommandException {
		
		String result = "EID\tState\tOpen\tStarting\tEnding\n";
		
		Set<Election> elections = facade.getElections();
		
		for (Election election: elections)
			result+=
				election.getEID()+"\t"+
				election.getElectionState()+"\t"+
				election.getOpenNominations()+"\t"+
				election.getStart()+"\t"+
				election.getEnd()+"\n";
		return result;
	}

}
