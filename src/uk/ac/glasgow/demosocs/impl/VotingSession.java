package uk.ac.glasgow.demosocs.impl;

import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.impl.users.User;

/**
 * Holds information about the
 * current session, ie.:
 * 	<li>current user</li>
 * 	<li>current election</li>
 * @author 0907390m
 *
 */
public class VotingSession {

	private User user;
	private Election election;
	
	
	public VotingSession(){
		user = null;
		election = null;
	}
	
	
	/**
	 * 
	 * @param GUID
	 */
	protected void setUser(String GUID){
		//TODO
		user = null;
	}
	
	protected void setElection(Election e){
		election = e;
	}
	
	/**
	 * 
	 * @return the current user's GUID
	 */
	protected String getUserGUID(){
//		return this.user.getGUID();
		return null;
	}
	
	protected Election getElection(){
		return this.election;
	}
}
