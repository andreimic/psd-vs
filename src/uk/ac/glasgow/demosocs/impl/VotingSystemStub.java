package uk.ac.glasgow.demosocs.impl;

import java.util.Date;
import java.util.Set;

import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.Vote;
import uk.ac.glasgow.demosocs.VotingSystem;
import uk.ac.glasgow.demosocs.impl.elections.ElectionModel;
import uk.ac.glasgow.demosocs.impl.elections.ElectionModelImpl;

public class VotingSystemStub implements VotingSystem {

	private VotingSession session;
//	private ElectionModelImpl electionModel;
	
	public VotingSystemStub(){
		session = new VotingSession();
	}
	
	
	@Override
	public void acceptNomination() {
		// TODO Auto-generated method stub

	}

	@Override
	public void castVote(Vote vote) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createElection(String name, String typeID) {

		// load a new ElectionModel, for fresh data
		ElectionModel em = new ElectionModelImpl();
		Election el = em.addElection(name, typeID);
		this.session.setElection(el);
	}

	@Override
	public void editElection(Date openNominations, Date start, Date end,
			String electoratelectionID) {

		// load a new ElectionModel, for fresh data		
		ElectionModel em = new ElectionModelImpl();
		em.editElection(
				this.session.getElection().getEID(),
				openNominations,
				start,
				end,
				electoratelectionID
		);
	}

	@Override
	public Set<Election> getElections() {

		// load a new ElectionModel, for fresh data
		ElectionModel em = new ElectionModelImpl();
		return em.getElections();
	}

	@Override
	public Set<Election> getNominations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void nominateCandidate(String GUID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void publishResults() {

		// load a new ElectionModel, for fresh data
		ElectionModel em = new ElectionModelImpl();
		em.publishResults(session.getElection().getEID());
	}

	@Override
	public void setCurrentElection(String electionID) {

		//get the election from the DB
		ElectionModel em = new ElectionModelImpl();
		Election el = em.getElection(electionID);
		this.session.setElection(el);
	}

	@Override
	public boolean setCurrentUser(String GUID, String password) {
		//authenticate
		//if auth OK, session.setUser(GUID)
		return false;
	}

	@Override
	public Election getCurrentElection() {
		return this.session.getElection();
	}

	@Override
	public String getCurrentUserGUID() {
		//TODO: check for NULLs?
		return this.session.getUserGUID();
	}
}
