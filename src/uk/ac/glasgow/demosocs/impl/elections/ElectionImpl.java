package uk.ac.glasgow.demosocs.impl.elections;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import uk.ac.glasgow.demosocs.Candidate;
import uk.ac.glasgow.demosocs.Election;
import uk.ac.glasgow.demosocs.ElectionResult;
import uk.ac.glasgow.demosocs.ElectionState;

public class ElectionImpl implements Election, Serializable {

	private String name;
	private String typeID;
	private String electionID;
	private String electorateID;

	private Date start;
	private Date openNominations;
	private Date end;
	
	private ElectionState electionState;
	
	public ElectionImpl(String name, String typeID, String electionID){
		this.name = name;
		this.typeID = typeID;
		this.electionID = electionID;
		
		this.electionState = ElectionState.NOT_STARTED;
	}
	
	@Override
	public String getEID() {
		return electionID;
	}

	@Override
	public ElectionResult getElectionResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ElectionState getElectionState() {
		return electionState;
	}

	@Override
	public Set<Candidate> getCandidates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getOpenNominations() {
		return openNominations;
	}

	@Override
	public Date getStart() {
		return start;
	}

	@Override
	public Date getEnd() {
		return end;
	}
	
	public void editElection(Date openNominations, Date start, Date end, String electoratelectionID){
		this.openNominations = (Date) openNominations.clone();
		this.start = (Date) start.clone();
		this.end = (Date) end.clone();
		
		this.electorateID = electoratelectionID;
	}

	
	public String toString(){
		return this.getEID() + " " + getOpenNominations() + " " + getStart() + " " + getEnd();
	}
}
