package uk.ac.glasgow.demosocs.impl.elections;

import java.util.Date;
import java.util.Set;

import uk.ac.glasgow.demosocs.Election;

public interface ElectionModel {

	/**
	 * Returns a Set of all the existing Elections.
	 * 
	 * @return
	 */
	public Set getElections();
	
	/**
	 * Returns an Election specified by its ID.
	 * 
	 * @param electionID
	 * @return
	 */
	public Election getElection(String electionID);
	
	/**
	 * Adds a new Election to the store.
	 * 
	 * @param name
	 * @param typeID
	 */
	public Election addElection(String name, String typeID);
	
	/**
	 * Edits the details of an Election.
	 * 
	 * @param electionID
	 * 			the ID of the election being edited
	 * @param openNominations
	 * @param start
	 * @param end
	 * @param electoratelectionID
	 */
	public void editElection(String electionID, Date openNominations, Date start, Date end, String electorateID);
	
	/**
	 * Returns a Set of GUIDs of the users that are
	 * part of the electorate of a particular Election.
	 * 
	 * @param electionID
	 * 			the ID of the Election
	 * @return
	 */
	public Set<String> getElectorate(String electionID);
	
	
	/**
	 * Publishes results for a particular election.
	 * 
	 * @param electionID
	 */
	public void publishResults(String electionID);
	public void vote();
}
