package uk.ac.glasgow.demosocs.impl.elections;

import java.util.Set;

public interface ElectorateModel {

	/**
	 * Returns a Set of all available Electorates.
	 * 
	 * @return
	 */
	public Set<Electorate> getElectorates();
	
	
	/**
	 * Returns a particular Electorate specified
	 * by its ID.
	 * 
	 * @param electorateID
	 * @return
	 */
	public Electorate getElectorate(String electorateID);
}
