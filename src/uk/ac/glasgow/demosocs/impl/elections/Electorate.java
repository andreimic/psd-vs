package uk.ac.glasgow.demosocs.impl.elections;

import java.util.Set;

import uk.ac.glasgow.demosocs.impl.users.User;

public class Electorate {

	private String electorateID;
	private Set<String> electorate;
	
	
	public Electorate(String electorateID, Set<String> electorate){
		this.electorateID = electorateID;
		this.electorate = electorate;
	}
	
	public Set<String> getElectorate(){
		return electorate;
	}
	
	public String getElectorateID(){
		return electorateID;
	}
	
	public String toString(){
		return this.electorateID + " " + electorate.toString();
	}
}
