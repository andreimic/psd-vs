package uk.ac.glasgow.demosocs.impl.elections;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import uk.ac.glasgow.demosocs.Election;

public class ElectionModelImpl implements ElectionModel {

	private String electionStore;
	private Set<Election> elections;
	
	public ElectionModelImpl(){
    	
    	elections = this.getStore();
    	
	}
	
	/**
	 * Loads the DB file and puts all the elections in the elections class member.
	 * 
	 * @return
	 * 		Set of all the existing Elections
	 */
	private Set<Election> getStore(){
		HashSet<Election> elections = new HashSet<Election>();
		
		Properties prop = new Properties();
		
		try {
    		prop.loadFromXML(new FileInputStream("src/uk/ac/glasgow/demosocs/impl/elections/election.properties"));
            electionStore = prop.getProperty("electionStore");
            
            File storeFile = new File(electionStore);
    		if (storeFile.exists()){
    			try {
    				ObjectInputStream ois = 
    					new ObjectInputStream(new FileInputStream(storeFile));
    				elections = (HashSet<Election>)ois.readObject();
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    			} catch (IOException e) {
    				e.printStackTrace();
    			} catch (ClassNotFoundException e) {
    				e.printStackTrace();
    			}
    		}
    		
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
		
		return elections;
	}

	@Override
	public Set<Election> getElections() {
		Set<Election> elections = getStore();
		return elections;
	}

	@Override
	public Election getElection(String electionID) {
		
		Set<Election>elections = getElections();

		for(Election el : elections)
			if(el.getEID().equals(electionID))
				return el;
		
		return null;
	}

	@Override
	public Election addElection(String name, String typeID) {

		Set<Election>elections = getElections();
		String electionID = Integer.toString(elections.size()+1);
		Election election = new ElectionImpl(name, typeID, electionID);
		
		elections.add(election);
		
		commitElections(elections);
		
		return election;
	}

	@Override
	public void editElection(String electionID, Date openNominations,
			Date start, Date end, String electorateID) {
		
		Set<Election> elections = getStore();
		
		for(Election el : elections){
			String elID = el.getEID();
			if(elID != null && elID.equals(electionID)){
				((ElectionImpl)el).editElection(openNominations, start, end, electorateID);
				break;	
			}
		}

		commitElections(elections);
	}

	@Override
	public Set<String> getElectorate(String electionID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void vote() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void publishResults(String electionID) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Writes the existing elections to the DB.
	 * 
	 * @param elections
	 */
	private void commitElections(Set<Election> elections){
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(electionStore));
			oos.writeObject(elections);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args){
//		ElectionModelImpl em = new ElectionModelImpl();
//		em.addElection("elname", "type1");
//		em.addElection("elname2", "type1");
//		em.addElection("elname3", "type1");
		
//		em.editElection("1", null, null, null, null);
		
//		Iterator<Election> elections = em.getElections().iterator();
//		while(elections.hasNext()){
//			Election el = elections.next();
//			System.out.println(el);
//		}
		
		
    	Properties prop = new Properties();
    	 
    	try {
 
    		prop.setProperty("electionStore", "src/uk/ac/glasgow/demosocs/impl/elections/elections.db");
    		prop.storeToXML(new FileOutputStream("src/uk/ac/glasgow/demosocs/impl/elections/electionStore.properties"), null);

    		prop.loadFromXML(new FileInputStream("src/uk/ac/glasgow/demosocs/impl/elections/electionStore.properties"));
            System.out.println(prop.getProperty("electionStore"));
 
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
	}
}
