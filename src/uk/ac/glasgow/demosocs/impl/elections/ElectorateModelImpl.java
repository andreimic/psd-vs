package uk.ac.glasgow.demosocs.impl.elections;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import uk.ac.glasgow.demosocs.Election;

public class ElectorateModelImpl implements ElectorateModel {

	private String electorateStore;
	private Set<Electorate> electorates;
	
	public ElectorateModelImpl(){
		electorates = this.getStore();
	}
	
	@Override
	public Set<Electorate> getElectorates() {
		this.electorates = getStore();
		return electorates;
	}

	@Override
	public Electorate getElectorate(String electorateID) {
		this.electorates = getStore();
		
		for(Electorate e : electorates)
			if(e.getElectorateID().equals(electorateID))
				return e;
		
		return null;
	}

	
	private Set<Electorate> getStore(){
		electorates = new HashSet<Electorate>();
		
		Properties prop = new Properties();
		
		try {
    		prop.loadFromXML(new FileInputStream("src/uk/ac/glasgow/demosocs/impl/elections/election.properties"));
            electorateStore = prop.getProperty("electorateStore");
            
            File storeFile = new File(electorateStore);
    		if (storeFile.exists()){
    			try {
    				ObjectInputStream ois = 
    					new ObjectInputStream(new FileInputStream(storeFile));
    				electorates = (HashSet<Electorate>)ois.readObject();
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    			} catch (IOException e) {
    				e.printStackTrace();
    			} catch (ClassNotFoundException e) {
    				e.printStackTrace();
    			}
    		}
    		
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
		
		return electorates;
	}
	
	
	public static void main(String[] args){
		
		String elID = "electorateID";
		Set<String> electorate = new HashSet<String>();
		
		electorate.add("A");
		electorate.add("B");
		electorate.add("C");
		
		Electorate el = new Electorate(elID, electorate);
		System.out.println(el);
		
		electorate = new HashSet<String>();
		electorate.add("X");
		electorate.add("Y");
		
		System.out.println(el);
		
		
//		Electorate el2 = new Electorate(elID, electorate);
//		System.out.println(el);
//		System.out.println(el2);
	}
}
