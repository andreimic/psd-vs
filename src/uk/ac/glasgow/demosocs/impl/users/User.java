package uk.ac.glasgow.demosocs.impl.users;

public class User {
	private String surname;
	private String forename;
	private String GUID;
	private String password;
	
	public User (String surname, String forename, String GUID, String password){
		this.surname = surname;
		this.forename = forename;
		this.GUID = GUID;
		this.password = password;
	}
	
	protected boolean authenticate (String password){
		return this.password.equals(password);
	}
}
