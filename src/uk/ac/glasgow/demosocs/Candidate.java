package uk.ac.glasgow.demosocs;

/**
 * Provides information about a candidate for an election.
 * @author tws
 *
 */
public interface Candidate extends Option {
	
	public String getGUID();
	
	public String getCandidateSurname();
	
	public String getCandidateForename();	
}
