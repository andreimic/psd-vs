package uk.ac.glasgow.demosocs;

public interface OneOfNVote extends Vote{
	public String getCandidateGUID();
}
