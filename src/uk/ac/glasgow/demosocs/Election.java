package uk.ac.glasgow.demosocs;

import java.util.Set;
import java.util.Date;

/**
 * Provides access to common information about election instances.
 */
public interface Election {
	
	/**
	 * @return the election unique identifier
	 */
	public String getEID();

	/**
	 * @return the result of the election if the state of the election has been
	 *         published, else null.
	 */
	public ElectionResult getElectionResult();

	/***
	 * @return the state of the election.
	 */
	public ElectionState getElectionState();

	/**
	 * @return the collection of confirmed candidates in the election.
	 */
	public Set<Candidate> getCandidates();

	/**
	 * @return the date the election is <i>scheduled</i> to open nominations.
	 */
	public Date getOpenNominations();

	/**
	 * @return the date the election is <i>scheduled</i> to start.
	 */
	public Date getStart();

	/**
	 * @return the date the election is <i>scheduled</i> to end.
	 */
	public Date getEnd();
}