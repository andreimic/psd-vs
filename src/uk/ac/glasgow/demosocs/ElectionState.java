package uk.ac.glasgow.demosocs;

public enum ElectionState {
	NOT_STARTED,
	NOMINATIONS_OPEN,
	STARTED,
	ENDED,
	PUBLISHED
}
