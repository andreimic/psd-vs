package uk.ac.glasgow.demosocs;

import java.util.Date;
import java.util.Set;

/**
 * Provides a <i>facade</i> over the architecture of the DemoSOCS voting system.
 * The facade is intended to be used by either a user interface component, or an
 * acceptance test harness. You must not modify the facade interface in any way.
 * 
 * @author tws
 * 
 */
public interface VotingSystem {

	/**
	 * Sets the current user, if the supplied GUID is for a valid user, and the
	 * password matches that of the chosen user. The user is then set as the
	 * current user of the system.
	 * 
	 * @param GUID
	 *            the Glasgow University Identifier of the user
	 * @param password
	 *            the candidate password for the user.
	 * @return true if the user was successfully authenticated, false otherwise.
	 */
	public boolean setCurrentUser(String GUID, String password);

	/**
	 * Sets the currently selected election to the specified ID, if the ID is
	 * valid. Does nothing if the ID is not valid.
	 * 
	 * @param electionID
	 *            the identifier of the election to be set.
	 */
	public void setCurrentElection(String electionID);

	/**
	 * 
	 * @return the set of elections managed by the voting system.
	 */
	public Set<Election> getElections();

	/**
	 * Creates a new election of the specified name and type, if the currently
	 * selected user is an election manager. The newly created election is
	 * automatically set as the current election.
	 * 
	 * @param name
	 * @param typeID
	 */
	public void createElection(String name, String typeID);

	/**
	 * Sets the details for the currently selected election.
	 * 
	 * @param openNominations
	 *            the date from which candidates may be nominated until the
	 *            election is started.
	 * @param start
	 *            the date the election starts and votes may be cast
	 * @param end
	 *            the date the election ends and no more votes may be cast.
	 * @param electoratelectionID
	 *            the identifier for the group of voters eligible to participate
	 *            in the election.
	 */
	public void editElection(Date openNominations, Date start, Date end,
			String electoratelectionID);

	/**
	 * Nominates a candidate to stand in the currently selected election, if:
	 * there is a currently selected election for which nominations are open;
	 * the GUID is for a valid user who is a member of the electorate for the
	 * election; and the currently selected user is a member of the electorate
	 * for the currently specified election.
	 * 
	 * @param GUID
	 *            the Glasgow University Identifier for the proposed candidate.
	 */
	public void nominateCandidate(String GUID);

	/**
	 * 
	 * @return The set of elections for which the currently selected user has
	 *         been nominated.
	 * 
	 */
	public Set<Election> getNominations();

	/**
	 * Accepts the nomination in the currently selected election for the
	 * currently selected user.
	 */
	public void acceptNomination();

	/**
	 * Casts the specified vote in the currently selected election for the
	 * currently selected user if: the user is a member of the electorate in the
	 * currently selected election.
	 * 
	 * @param casts
	 */
	public void castVote(Vote vote);

	/**
	 * Publishes the results of the currently selected election, if the election
	 * has ended and the currently selected user is the election manager for the
	 * currently selected election.
	 */
	public void publishResults();

	public Election getCurrentElection();

	public String getCurrentUserGUID();
}
